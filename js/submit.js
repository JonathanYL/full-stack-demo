$(document).ready(function() {
	var endpoint = "https://experimentation.getsnaptravel.com/interview/hotels";
	$("#submitButton").click(function(event) {
		clearTable();
		event.preventDefault();

		var cityVal = $("#city").val();
		var checkInVal = $("#checkin").val();
		var checkOutVal = $("#checkout").val();

		var snapTravelPost = $.post(endpoint, { city : cityVal, checkin : checkInVal, checkout: checkOutVal, provider: 'snaptravel' });
		var hotelsComPost = $.post(endpoint, { city : cityVal, checkin : checkInVal, checkout: checkOutVal, provider: 'retail' });

		var snapTravelResponseArray;
		var hotelsComResponseArray;

		snapTravelPost.done(function(data) {
			snapTravelResponseArray = data["hotels"];
		});

		hotelsComPost.done(function(data) {
			hotelsComResponseArray = data["hotels"];
		});

		$.when(snapTravelPost, hotelsComPost).done(function() {
			snapTravelResponseArray = filterArray(snapTravelResponseArray, hotelsComResponseArray).sort(function(a,b) {
     									return a.id > b.id;
 									  });
			hotelsComResponseArray = filterArray(hotelsComResponseArray, snapTravelResponseArray).sort(function(a,b) {
     									return a.id > b.id;
 									  });;

			console.log(snapTravelResponseArray);
			console.log(hotelsComResponseArray);
			createTable(snapTravelResponseArray, hotelsComResponseArray);
		});
  	});
});

function filterArray(first, second) {
    return first.filter(function(o) {
    			return second.some(function(o2) {
        		return o.id === o2.id;
    			})
			});
}

function clearTable() {
	$("#resultsTable tr").remove(); 
	$("#resultsTable th").remove(); 
}

function createTable(snapTravelArray, hotelsComArray) {
	var table = $("#resultsTable");
	table.append('<tr>');
	table.append('<th> Hotel<br> id </th>');
	table.append('<th> Hotel<br> Name </th>');
	table.append('<th> Number of<br> Reviews </th>');
	table.append('<th> Address </th>');
	table.append('<th> Number of<br> Stars </th>');
	table.append('<th> Amenities </th>');
	table.append('<th> Image </th>');
	table.append('<th> Snaptravel<br> Price </th>');
	table.append('<th> Hotel.com<br> Price </th>');
	table.append('</tr>');
	for (var i = 0; i < snapTravelArray.length; i++) {
		var snapTravelHotel = snapTravelArray[i];
		var hotelComHotel = hotelsComArray[i];

		table.append('<tr>');
		table.append('<th>' + snapTravelHotel.id + '</th>');
		table.append('<th>' + snapTravelHotel.hotel_name + '</th>');
		table.append('<th>' + snapTravelHotel.num_reviews + '</th>');
		table.append('<th>' + snapTravelHotel.address + '</th>');
		table.append('<th>' + snapTravelHotel.num_stars + '</th>');
		table.append('<th>' + snapTravelHotel.amenities.join(",") + '</th>');
		table.append('<th>' + '<img style="width:100px" src="' +  snapTravelHotel.image_url + '">' + '</th>');
		table.append('<th>' + snapTravelHotel.price + '</th>');
		table.append('<th>' + hotelComHotel.price + '</th>');
		table.append('</tr>');
	}
}

